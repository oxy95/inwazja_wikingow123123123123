#pragma once
#include <string>
#include <time.h>
using namespace std;
class Karta
{
public:
	int id_karty;
	int wartosc_karty;
	string kolor_karty;
	string znak_karty;
	Karta * Next;
	Karta * Prev;
	void WstawNaPoczatek(Karta* &Head, Karta* &Tail, int id, int wartosc, string kolor, string znak);
	void WstawNaKoniec(Karta* &Tail, int id, int wartosc, string kolor, string znak);
	void TasujTalie(Karta* &Head, Karta* &Tail);
	Karta();
	~Karta();
};

